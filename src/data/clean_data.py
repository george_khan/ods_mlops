import pandas as pd
import numpy as np
import click


def fillna_col_num(df: pd.DataFrame, val: float, columns: list) -> pd.DataFrame:
    """
    Функция заполнения пропуска числовой фичи
    :param df: датафрейм для замены
    :param val: значение для замены
    :param columns: список полей для замены
    :return: датафрейм
    """
    for col in columns:
        df.loc[:, col] = df.loc[:, col].fillna(val)
    return df


def fillna_col_cat(df: pd.DataFrame, val: str, columns: list) -> pd.DataFrame:
    """
    Функция заполнения пропуска категориальной фичи
    :param df: датафрейм для замены
    :param val: значение для замены
    :param columns: список полей для замены
    :return: датафрейм
    """
    for col in columns:
        df.loc[:, col] = df.loc[:, col].fillna(val)
    return df


def clean_num_features(x_train: pd.DataFrame, x_test: pd.DataFrame):
    # замена пропусков на 0
    x_train = fillna_col_num(
        x_train,
        0,
        [
            "BsmtFinSF1",
            "BsmtFinSF2",
            "BsmtFullBath",
            "BsmtHalfBath",
            "BsmtUnfSF",
            "GarageArea",
            "GarageCars",
            "GarageYrBlt",
            "MasVnrArea",
        ],
    )
    x_test = fillna_col_num(
        x_test,
        0,
        [
            "BsmtFinSF1",
            "BsmtFinSF2",
            "BsmtFullBath",
            "BsmtHalfBath",
            "BsmtUnfSF",
            "GarageArea",
            "GarageCars",
            "GarageYrBlt",
            "MasVnrArea",
        ],
    )

    # замена пропуска на median
    x_train = fillna_col_num(
        x_train, x_train[["TotalBsmtSF"]].median().values[0], ["TotalBsmtSF"]
    )
    x_test = fillna_col_num(
        x_test, x_test[["TotalBsmtSF"]].median().values[0], ["TotalBsmtSF"]
    )

    x_train = fillna_col_num(
        x_train, x_train[["LotFrontage"]].median().values[0], ["LotFrontage"]
    )
    x_test = fillna_col_num(
        x_test, x_test[["LotFrontage"]].median().values[0], ["LotFrontage"]
    )

    # Работа с категориальными празнаками, которые обозначают качество
    for col in [
        "ExterQual",
        "ExterCond",
        "HeatingQC",
        "KitchenQual",
        "FireplaceQu",
        "GarageQual",
        "GarageCond",
        "PoolQC",
        "BsmtQual",
        "BsmtCond",
    ]:
        x_train[col] = x_train[col].map(
            {"None": 0, "Po": 1, "Fa": 2, "TA": 3, "Gd": 4, "Ex": 5}
        )
        x_test[col] = x_test[col].map(
            {"None": 0, "Po": 1, "Fa": 2, "TA": 3, "Gd": 4, "Ex": 5}
        )

    x_train["BsmtExposure"] = x_train["BsmtExposure"].map(
        {"None": 0, "No": 1, "Mn": 2, "Av": 3, "Gd": 4}
    )
    x_test["BsmtExposure"] = x_test["BsmtExposure"].map(
        {"None": 0, "No": 1, "Mn": 2, "Av": 3, "Gd": 4}
    )
    x_train["BsmtFinType1"] = x_train["BsmtFinType1"].map(
        {"None": 0, "Unf": 1, "LwQ": 2, "Rec": 3, "BLQ": 4, "ALQ": 5, "GLQ": 6}
    )
    x_train["BsmtFinType2"] = x_train["BsmtFinType2"].map(
        {"None": 0, "Unf": 1, "LwQ": 2, "Rec": 3, "BLQ": 4, "ALQ": 5, "GLQ": 6}
    )

    x_test["BsmtFinType1"] = x_test["BsmtFinType1"].map(
        {"None": 0, "Unf": 1, "LwQ": 2, "Rec": 3, "BLQ": 4, "ALQ": 5, "GLQ": 6}
    )
    x_test["BsmtFinType2"] = x_test["BsmtFinType2"].map(
        {"None": 0, "Unf": 1, "LwQ": 2, "Rec": 3, "BLQ": 4, "ALQ": 5, "GLQ": 6}
    )

    # тут не такие очевидные категории оценкци, которые эвристически можно
    # приоритезировать
    x_train["LandSlope"] = x_train["LandSlope"].map({"Sev": 1, "Mod": 2, "Gtl": 3})

    x_train["PavedDrive"] = x_train["PavedDrive"].map({"N": 1, "P": 2, "Y": 3})

    x_train["Functional"] = x_train["Functional"].map(
        {
            "Sal": 1,
            "Sev": 2,
            "Maj2": 3,
            "Maj1": 4,
            "Mod": 5,
            "Min2": 6,
            "Min1": 7,
            "Typ": 8,
        }
    )
    x_train["Street"] = x_train["Street"].map({"Grvl": 1, "Pave": 2})

    x_train["Utilities"] = x_train["Utilities"].map(
        {"ELO": 1, "NoSeWa": 2, "NoSewr": 3, "AllPub": 4}
    )

    x_test["LandSlope"] = x_test["LandSlope"].map({"Sev": 1, "Mod": 2, "Gtl": 3})

    x_test["PavedDrive"] = x_test["PavedDrive"].map({"N": 1, "P": 2, "Y": 3})

    x_test["Functional"] = x_test["Functional"].map(
        {
            "Sal": 1,
            "Sev": 2,
            "Maj2": 3,
            "Maj1": 4,
            "Mod": 5,
            "Min2": 6,
            "Min1": 7,
            "Typ": 8,
        }
    )
    x_test["Street"] = x_test["Street"].map({"Grvl": 1, "Pave": 2})

    x_test["Utilities"] = x_test["Utilities"].map(
        {"ELO": 1, "NoSeWa": 2, "NoSewr": 3, "AllPub": 4}
    )


def clean_cat_features(x_train: pd.DataFrame, x_test: pd.DataFrame):
    x_test.loc[1116, "GarageType"] = np.nan

    # замена на "None"
    x_train = fillna_col_cat(
        x_train,
        "None",
        [
            "BsmtCond",
            "BsmtExposure",
            "BsmtFinType1",
            "BsmtFinType2",
            "BsmtQual",
            "Alley",
            "Fence",
            "FireplaceQu",
            "GarageCond",
            "GarageFinish",
            "GarageQual",
            "GarageType",
            "MasVnrType",
            "MiscFeature",
            "PoolQC",
        ],
    )
    x_test = fillna_col_cat(
        x_test,
        "None",
        [
            "BsmtCond",
            "BsmtExposure",
            "BsmtFinType1",
            "BsmtFinType2",
            "BsmtQual",
            "Alley",
            "Fence",
            "FireplaceQu",
            "GarageCond",
            "GarageFinish",
            "GarageQual",
            "GarageType",
            "MasVnrType",
            "MiscFeature",
            "PoolQC",
        ],
    )

    # замена пропуска на moda
    for col in [
        "Electrical",
        "Exterior1st",
        "Exterior2nd",
        "Functional",
        "KitchenQual",
        "SaleType",
        "Utilities",
    ]:
        x_train = fillna_col_cat(x_train, x_train[[col]].mode().values[0, 0], [col])
        x_test = fillna_col_cat(x_test, x_test[[col]].mode().values[0, 0], [col])

    # 4 пропуска в тесте, поэтому заменим на моду, но в разрезе локации Neighborhood
    for i in x_test["Neighborhood"].unique():
        if x_test.MSZoning[x_test["Neighborhood"] == i].isnull().sum() > 0:
            x_test.loc[x_test["Neighborhood"] == i, "MSZoning"] = x_test.loc[
                x_test["Neighborhood"] == i, "MSZoning"
            ].fillna(x_test.loc[x_test["Neighborhood"] == i, "MSZoning"].mode()[0])

    # это категориальная фича, переведем ее в текст, чтобы потом быстро
    # отделить категории от чисел
    x_train["MSSubClass"] = x_train["MSSubClass"].map(
        {
            20: "class1",
            30: "class2",
            40: "class3",
            45: "class4",
            50: "class5",
            60: "class6",
            70: "class7",
            75: "class8",
            80: "class9",
            85: "class10",
            90: "class11",
            120: "class12",
            150: "class13",
            160: "class14",
            180: "class15",
            190: "class16",
        }
    )
    x_test["MSSubClass"] = x_test["MSSubClass"].map(
        {
            20: "class1",
            30: "class2",
            40: "class3",
            45: "class4",
            50: "class5",
            60: "class6",
            70: "class7",
            75: "class8",
            80: "class9",
            85: "class10",
            90: "class11",
            120: "class12",
            150: "class13",
            160: "class14",
            180: "class15",
            190: "class16",
        }
    )


@click.command()
@click.argument("train_path", type=click.Path(exists=True))
@click.argument("test_path", type=click.Path(exists=True))
def clean_data(train_path: str, test_path: str):
    x_train = pd.read_csv(train_path)
    x_test = pd.read_csv(test_path)
    clean_num_features(x_train, x_test)
    clean_cat_features(x_train, x_test)
    x_train.to_csv("data/interim/train_clean.csv", index_label=False)
    x_test.to_csv("data/interim/test_clean.csv", index_label=False)


if __name__ == "__main__":
    clean_data()
