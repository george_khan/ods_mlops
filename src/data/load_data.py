import pandas as pd
import click


@click.command()
@click.argument("train_path_in", type=click.Path(exists=True))
@click.argument("test_path_in", type=click.Path(exists=True))
def load_data(train_path_in: str, test_path_in: str):
    df_train = pd.read_csv(train_path_in)
    df_test = pd.read_csv(test_path_in)
    # создаем общий фрейм для подготовки данных
    x_train = df_train.drop(["Id", "SalePrice"], axis=1)
    x_test = df_test.drop(["Id"], axis=1)
    y_train = df_train[["SalePrice"]]
    x_test_idx = df_test["Id"]

    x_train.to_csv("data/interim/train.csv", index_label=False)
    x_test.to_csv("data/interim/test.csv", index_label=False)
    y_train.to_csv("data/processed/target.csv", index_label=False)
    x_test_idx.to_csv("data/processed/test_idx.csv", index_label=False)


if __name__ == "__main__":
    load_data()
