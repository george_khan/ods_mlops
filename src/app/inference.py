import pandas as pd
import mlflow
import os
from dotenv import load_dotenv
import unicorn
from fastapi import FastAPI, File, UploadFile, HTTPException

load_dotenv()

app = FastAPI()

os.environ["MLFLOW_S3_ENDPOINT_URL"] = str(os.getenv("MLFLOW_S3_ENDPOINT_URL"))


class Model:
    def __init__(self, model_name: str, model_stage: str):
        self.model = mlflow.pyfunc.log_model("models:/{model_name}/{model_stage}")

    def predict(self, data: pd.DataFrame):
        pred = self.model.predict(data)
        return pred


model = Model("KernelRidge", "Staging")


@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    if file.filename.endswith(".csv"):
        with open(file.filename, "wb") as f:
            f.write(file.file.read())
        data = pd.read_csv(file.filename)
        os.remove(file.filename)
        return list(model.predict(data))
    else:
        raise HTTPException(
            status_code=400, detail="Invalid file format. Only csv file accepted"
        )


if os.getenv("AWS_ACCESS_KEY_ID") is None or os.getenv("AWS_SECRET_ACCESS_KEY") is None:
    exit(1)
