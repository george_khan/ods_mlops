import pickle as pck
from sklearn.metrics import mean_squared_error
import numpy as np
import pandas as pd


def save(obj, file_name: str):
    with open("{}.pickle".format(file_name), "wb") as f:
        pck.dump(obj, f)


def load(file_name: str):
    with open("{}".format(file_name), "rb") as f:
        return pck.load(f)


def neg_rmse(y_true: np.array, y_pred: np.array) -> float:
    return -1.0 * np.sqrt(mean_squared_error(y_true, y_pred))


# функция для записи прогнозов в файл
def write_to_submission_file(
    idx,
    predicted_labels,
    file_name,
):
    target = "SalePrice"
    index_label = "Id"
    path = "data/processed/{}.csv"
    predicted_df = pd.DataFrame(
        predicted_labels,
        index=idx,
        columns=[target],
    )
    predicted_df.to_csv(path.format(file_name), index_label=index_label)
