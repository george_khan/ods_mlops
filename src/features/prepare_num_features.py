import pandas as pd
from sklearn.preprocessing import StandardScaler
import numpy as np
import click


def df_scale(x_train: pd.DataFrame, x_test: pd.DataFrame) -> np.array:
    """
    Маштабирование данных, на выходе получим numpy array
    :param x_train:
    :param x_test:
    :return:
    """
    scaler = StandardScaler()
    x_train = scaler.fit_transform(x_train)
    x_test = scaler.transform(x_test)
    return x_train, x_test


@click.command()
@click.argument("train_path", type=click.Path(exists=True))
@click.argument("test_path", type=click.Path(exists=True))
def prepare_num_features(train_path: str, test_path: str):
    x_train = pd.read_csv(train_path)
    x_test = pd.read_csv(test_path)

    # выделим числовые фичи
    x_train_num = x_train.select_dtypes(include="number")
    x_test_num = x_test.select_dtypes(include="number")

    # отмасштабируем числовые признаки
    x_train_num_scale, x_test_num_scale = df_scale(x_train_num, x_test_num)

    X_train_num_scale = pd.DataFrame(
        data=x_train_num_scale, columns=x_train_num.columns
    )
    x_test_num_scale = pd.DataFrame(data=x_test_num_scale, columns=x_test_num.columns)

    X_train_num_scale.to_csv("data/interim/train_prepared_num.csv", index_label=False)
    x_test_num_scale.to_csv("data/interim/test_prepared_num.csv", index_label=False)


if __name__ == "__main__":
    prepare_num_features()
