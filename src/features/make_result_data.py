import pandas as pd
import numpy as np
import click
import pickle as pck


def save(obj, file_name: str):
    with open("{}.pickle".format(file_name), "wb") as f:
        pck.dump(obj, f)


def load(file_name: str):
    with open("{}".format(file_name), "rb") as f:
        return pck.load(f)


@click.command()
@click.argument("train_path_num", type=click.Path(exists=True))
@click.argument("train_path_cat", type=click.Path(exists=True))
@click.argument("test_path_num", type=click.Path(exists=True))
@click.argument("test_path_cat", type=click.Path(exists=True))
def make_result_data(
    train_path_num: str, train_path_cat: str, test_path_num: str, test_path_cat: str
):
    x_train_num = pd.read_csv(train_path_num)
    x_train_cat = load(train_path_cat)
    x_test_num = pd.read_csv(test_path_num)
    x_test_cat = load(test_path_cat)

    x_train = np.hstack([x_train_num, x_train_cat])
    x_test = np.hstack([x_test_num, x_test_cat])

    save(x_train, "data/processed/train")
    save(x_test, "data/processed/test")


if __name__ == "__main__":
    make_result_data()
