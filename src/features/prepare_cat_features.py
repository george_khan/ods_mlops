import pandas as pd
import numpy as np
import click
import pickle as pck


def save(obj, file_name: str):
    with open("{}.pickle".format(file_name), "wb") as f:
        pck.dump(obj, f)


def load(file_name: str):
    with open(file_name, "rb") as f:
        return pck.load(f)


def get_hash_data(df: pd.DataFrame, hash_space: int) -> np.array:
    """
    Функция для реализации Hashing trick
    :param df: датафрейм с категориальными фичами
    :param hash_space: размерность результирущей разряженной матрицы
    :return: разряженная матрица по категориальным фичам
    """
    np.zeros([df.shape[0], hash_space])
    x_hash = pd.DataFrame(np.zeros([df.shape[0], hash_space]))
    for i, row in df.iterrows():
        for col, val in row.items():
            calc_col = hash(col + "." + str(val)) % hash_space
            x_hash.loc[i, calc_col] = 1
    return x_hash


@click.command()
@click.argument("train_path", type=click.Path(exists=True))
@click.argument("test_path", type=click.Path(exists=True))
def prepare_cat_features(train_path: str, test_path: str):
    x_train = pd.read_csv(train_path)
    x_test = pd.read_csv(test_path)

    # выделим категориальные фичи
    x_train_cat = x_train.select_dtypes(exclude="number")
    x_test_cat = x_test.select_dtypes(exclude="number")

    # закодируем категориальные признаки через Hashing trick, предполагая,
    # что в каждой фиче 10 параметров. Возможно можно сузить
    x_train_cat_hash = get_hash_data(x_train_cat, x_train_cat.shape[1] * 10)
    x_test_cat_hash = get_hash_data(x_test_cat, x_train_cat.shape[1] * 10)

    save(x_train_cat_hash, "data/interim/train_prepared_cat")
    save(x_test_cat_hash, "data/interim/test_prepared_cat")


if __name__ == "__main__":
    prepare_cat_features()
