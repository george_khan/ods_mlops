import pandas as pd
import os
import numpy as np

# from src.tools import neg_rmse, save, load, write_to_submission_file
from sklearn.linear_model import (
    LassoCV,
    Ridge,
    RidgeCV,
)
from xgboost import XGBRegressor

from sklearn.kernel_ridge import KernelRidge
from sklearn.ensemble import RandomForestRegressor, StackingRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
import joblib as jb
from sklearn.metrics import make_scorer, mean_squared_error
import click
import pickle as pck
import mlflow
from mlflow.models.signature import infer_signature
from dotenv import load_dotenv


load_dotenv()  # загрузка переменных сред файла .env


# os.environ['AWS_ACCESS_KEY_ID']='minioadmin'
# os.environ['AWS_SECRET_ACCESS_KEY']='minioadmin'

remote_service_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(remote_service_uri)  # определяет адрес mlflow сервиса
# mlflow.autolog() # включает автолог запусков

mlflow.set_experiment("Stacking")


def save(obj, file_name: str):
    with open("{}.pickle".format(file_name), "wb") as f:
        pck.dump(obj, f)


def load(file_name: str):
    with open("{}".format(file_name), "rb") as f:
        return pck.load(f)


def neg_rmse(y_true: np.array, y_pred: np.array) -> float:
    return -1.0 * np.sqrt(mean_squared_error(y_true, y_pred))


# функция для записи прогнозов в файл
def write_to_submission_file(
    idx,
    predicted_labels,
    file_name,
):
    target = "SalePrice"
    index_label = "Id"
    path = "data/processed/{}.csv"
    predicted_df = pd.DataFrame(
        predicted_labels,
        index=idx,
        columns=[target],
    )
    predicted_df.to_csv(path.format(file_name), index_label=index_label)


@click.command()
@click.argument("train_path", type=click.Path(exists=True))
@click.argument("test_path", type=click.Path(exists=True))
@click.argument("target_path", type=click.Path(exists=True))
@click.argument("test_idx", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path(exists=False))
def fit_model(
    train_path: str, test_path: str, target_path: str, test_idx: str, output_path: str
):
    with mlflow.start_run():  # контекстный менеджер для эксперимента
        mlflow.get_artifact_uri()

        print(mlflow.get_artifact_uri())

        x_train = load(train_path)
        x_test = load(test_path)
        x_test_idx = pd.read_csv(test_idx)
        y_train = pd.read_csv(target_path)
        y_train_log = np.log(y_train)

        neg_rmse_score = make_scorer(neg_rmse)
        # фиксируем параметры
        params = dict()
        cv = 5
        random_state = 27
        scoring = neg_rmse_score
        params["lasso"] = {
            "alphas": [0.00030888435964774815],
            "cv": cv,
            "random_state": random_state,
        }
        params["ridge"] = {"alphas": [12.915], "cv": cv, "scoring": scoring}
        params["kernel_ridge"] = {
            "alpha": 1.2777777777777777,
            "coef0": 3.7888888888888888,
            "degree": 3,
            "kernel": "polynomial",
        }
        params["svr"] = {"C": 0.6158482110660264, "epsilon": 0.0001291549665014884}
        params["knn"] = {"metric": "euclidean", "n_neighbors": 15}
        params["rf"] = {
            "max_depth": 13,
            "max_features": 0.4,
            "max_samples": 0.9,
            "min_impurity_decrease": 0,
            "min_samples_leaf": 1,
            "min_samples_split": 2,
            "n_estimators": 550,
            "random_state": random_state,
        }
        params["xgb"] = {
            "colsample_bylevel": 0.9,
            "colsample_bytree": 0.7,
            "gamma": 0.01,
            "learning_rate": 0.02111111111111111,
            "max_depth": 6,
            "min_child_weight": 4,
            "n_estimators": 1180,
            "reg_alpha": 0.01,
            "reg_lambda": 0.01,
            "subsample": 0.9,
        }
        params["final_estimator"] = {"final_estimator__alpha": np.logspace(-3, 0, 15)}
        # формируем базовые модели
        models_l0 = [
            ("lasso", LassoCV(**params["lasso"])),
            ("ridge", RidgeCV(**params["ridge"])),
            ("kernel_ridge", KernelRidge(**params["kernel_ridge"])),
            ("svr", SVR(**params["svr"])),
            ("knn", KNeighborsRegressor(**params["knn"])),
            ("rf", RandomForestRegressor(**params["rf"])),
            ("xgb", XGBRegressor(**params["xgb"])),
        ]
        model_l1 = Ridge(alpha=0.13894954943731375)

        model_stack = StackingRegressor(
            estimators=models_l0, final_estimator=model_l1, cv=5
        )
        # gs = GridSearchCV(estimator=model_stack, param_grid=params['final_estimator'],
        #                   verbose=2, cv=5, n_jobs=-1, scoring=neg_rmse_score)
        # gs.fit(X_train_all, y_train_log)
        # print(gs.best_score_)
        # print(gs.best_params_)
        model_stack.fit(x_train, y_train_log)
        predict_train_log = model_stack.predict(x_train)

        jb.dump(model_stack, output_path)
        mlflow.log_params(params)

        score = dict(
            mean_squared_error_train=mean_squared_error(
                y_train, np.exp(predict_train_log)
            ),
            mean_squared_log_error_train=neg_rmse(y_train_log, predict_train_log),
        )

        mlflow.log_metrics(score)  # лог метрик
        # mlflow.log_artifacts(output_path) # логирование артефакта
        preds = np.exp(model_stack.predict(x_test))
        signature = infer_signature(x_test, preds)
        mlflow.sklearn.log_model(
            sk_model=model_stack,
            artifact_path=output_path,
            registered_model_name="Stacking",
            signature=signature,
        )  # логирование модели

        pred = np.exp(model_stack.predict(x_test))
        write_to_submission_file(x_test_idx["Id"].values, pred, "house_price_stacking")


if __name__ == "__main__":
    fit_model()
