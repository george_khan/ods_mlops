import pandas as pd
import os
import numpy as np
# from src.tools import neg_rmse, save, load, write_to_submission_file
import joblib as jb
from sklearn.metrics import mean_squared_error
from xgboost import XGBRegressor
import click
import pickle as pck
import mlflow
from mlflow.models.signature import infer_signature
from dotenv import load_dotenv

load_dotenv()  # загрузка переменных сред файла .env

# os.environ['AWS_ACCESS_KEY_ID']='minioadmin'
# os.environ['AWS_SECRET_ACCESS_KEY']='minioadmin'

remote_service_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(remote_service_uri)  # определяет адрес mlflow сервиса
# mlflow.autolog() # включает автолог запусков

mlflow.set_experiment("XGBRegressor")


def save(obj, file_name: str):
    with open("{}.pickle".format(file_name), "wb") as f:
        pck.dump(obj, f)


def load(file_name: str):
    with open("{}".format(file_name), "rb") as f:
        return pck.load(f)


def neg_rmse(y_true: np.array, y_pred: np.array) -> float:
    return -1.0 * np.sqrt(mean_squared_error(y_true, y_pred))


# функция для записи прогнозов в файл
def write_to_submission_file(
    idx,
    predicted_labels,
    file_name,
):
    target = "SalePrice"
    index_label = "Id"
    path = "data/processed/{}.csv"
    predicted_df = pd.DataFrame(
        predicted_labels,
        index=idx,
        columns=[target],
    )
    predicted_df.to_csv(path.format(file_name), index_label=index_label)


@click.command()
@click.argument("train_path", type=click.Path(exists=True))
@click.argument("test_path", type=click.Path(exists=True))
@click.argument("target_path", type=click.Path(exists=True))
@click.argument("test_idx", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path(exists=False))
def fit_model(
    train_path: str, test_path: str, target_path: str, test_idx: str, output_path: str
):
    with mlflow.start_run():  # контекстный менеджер для эксперимента
        mlflow.get_artifact_uri()

        print(mlflow.get_artifact_uri())

        x_train = load(train_path)
        x_test = load(test_path)
        x_test_idx = pd.read_csv(test_idx)
        y_train = pd.read_csv(target_path)
        y_train_log = np.log(y_train)

        params = {
            "colsample_bylevel": 0.9,
            "colsample_bytree": 0.7,
            "gamma": 0.01,
            "learning_rate": 0.02111111111111111,
            "max_depth": 6,
            "min_child_weight": 4,
            "n_estimators": 1180,
            "reg_alpha": 0.01,
            "reg_lambda": 0.01,
            "subsample": 0.9,
            "random_state": 27,
        }
        gbr = XGBRegressor(**params)
        gbr.fit(x_train, y_train_log)

        jb.dump(gbr, output_path)
        mlflow.log_params(params)
        predict_train_log = gbr.predict(x_train)

        score = dict(
            mean_squared_error_train=mean_squared_error(
                y_train, np.exp(predict_train_log)
            ),
            mean_squared_log_error_train=neg_rmse(y_train_log, predict_train_log),
        )

        mlflow.log_metrics(score)  # лог метрик
        # mlflow.log_artifacts(output_path) # логирование артефакта
        preds = np.exp(gbr.predict(x_test))
        signature = infer_signature(x_test, preds)
        mlflow.sklearn.log_model(
            sk_model=gbr,
            artifact_path=output_path,
            registered_model_name="XGBRegressor",
            signature=signature,
        )  # логирование модели

        pred = np.exp(gbr.predict(x_test))
        write_to_submission_file(x_test_idx["Id"].values, pred, "house_price_xgb")


if __name__ == "__main__":
    fit_model()
