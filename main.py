# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool
# windows, actions, and settings.
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
# OneHotEncoder
# from sklearn.linear_model import (
#     LinearRegression,
#     Lasso,
#     LassoCV,
#     Ridge,
#     RidgeCV,
#     SGDRegressor,
# )
# from sklearn.kernel_ridge import KernelRidge
# from sklearn.ensemble import RandomForestRegressor, StackingRegressor
# from sklearn.neighbors import KNeighborsRegressor
# from sklearn.svm import SVC, SVR
#
# from sklearn.model_selection import GridSearchCV, cross_val_score, train_test_split
# from sklearn.metrics import mean_squared_log_error, make_scorer, mean_squared_error
# import xgboost as xgb
# from scipy import stats
#
# import seaborn as sns
# import matplotlib.pyplot as plt

# вспомагательные функции


def fillna_col_val(X, val, columns):
    for col in columns:
        X.loc[:, col] = X.loc[:, col].fillna(val)
    return X


def fillna_col(X_train):
    for col in X_train.columns:
        X_train.loc[:, col] = X_train.loc[:, col].fillna(X_train[col].median())
    return X_train


# #маштабирование данных, на выходе получим numpy array
def df_scale(X_train, X_test):
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    return X_train, X_test


def prepare_data(X_train, X_test):
    X_train = fillna_col(X_train)
    X_test = fillna_col(X_test)
    X_train_scale, X_test_scale = df_scale(X_train, X_test)
    return X_train_scale, X_test_scale


# функция для записи прогнозов в файл
def write_to_submission_file(
    idx, predicted_labels, out_file, target="SalePrice", index_label="Id"
):
    predicted_df = pd.DataFrame(
        predicted_labels,
        index=idx,
        columns=[target],
    )
    predicted_df.to_csv(out_file, index_label=index_label)


# hashing trick
def get_hash_data(X, hash_space):
    np.zeros([X.shape[0], hash_space])
    X_hash = pd.DataFrame(np.zeros([X.shape[0], hash_space]))
    for i, row in X.iterrows():
        for col, val in row.items():
            calc_col = hash(col + "." + str(val)) % hash_space
            X_hash.loc[i, calc_col] = 1
    return X_hash


def show_uniq(col, df):
    print(df[col].unique())


# show unique values in the columns
def show_uniqs(df):
    for col in df.columns:
        print(col)
        show_uniq(col, df)
        print("=======================================")


def load_data():
    df_train = pd.read_csv("train.csv")
    df_test = pd.read_csv("test.csv")
    print(df_train.shape, df_test.shape)


def prepare_data_main(df_train, df_test):
    X_train = df_train.drop(["Id", "SalePrice"], axis=1)
    X_test = df_test.drop(["Id"], axis=1)
    df_all = X_train.append(X_test)
    # почти все числовые признаки подвала связанны с категориальными
    # признаками подвала как 0-NaN NaN-NaN
    # заменим все категории на "None", а числа на 0
    df_all[df_all["BsmtQual"].isnull()][
        [
            "BsmtFinSF1",
            "BsmtFinSF2",
            "BsmtFullBath",
            "BsmtHalfBath",
            "BsmtUnfSF",
            "BsmtCond",
            "BsmtExposure",
            "BsmtFinType1",
            "BsmtFinType2",
            "BsmtQual",
        ]
    ]  # (81, 10)
    df_all[df_all["BsmtCond"] == "None"][
        [
            "BsmtFinSF1",
            "BsmtFinSF2",
            "BsmtFullBath",
            "BsmtHalfBath",
            "BsmtUnfSF",
            "BsmtCond",
            "BsmtExposure",
            "BsmtFinType1",
            "BsmtFinType2",
            "BsmtQual",
        ]
    ]  # .shape #(37, 10)
    X_train = fillna_col_val(
        X_train,
        0,
        ["BsmtFinSF1", "BsmtFinSF2", "BsmtFullBath", "BsmtHalfBath", "BsmtUnfSF"],
    )
    X_train = fillna_col_val(
        X_train,
        "None",
        ["BsmtCond", "BsmtExposure", "BsmtFinType1", "BsmtFinType2", "BsmtQual"],
    )

    X_test = fillna_col_val(
        X_test,
        0,
        ["BsmtFinSF1", "BsmtFinSF2", "BsmtFullBath", "BsmtHalfBath", "BsmtUnfSF"],
    )
    X_test = fillna_col_val(
        X_test,
        "None",
        ["BsmtCond", "BsmtExposure", "BsmtFinType1", "BsmtFinType2", "BsmtQual"],
    )

    # Alley
    print(
        df_all[["Alley"]].isnull().sum()
    )  # 2721 пропуск, в справочнике есть NaN, заменим на 'None'
    X_train = fillna_col_val(X_train, "None", ["Alley"])
    X_test = fillna_col_val(X_test, "None", ["Alley"])

    # Electrical
    print(
        df_all[["Electrical"]].isnull().sum()
    )  # один пропуск, поэтому просто заменим на моду
    X_train = fillna_col_val(
        X_train, X_train[["Electrical"]].mode().values[0, 0], ["Electrical"]
    )
    X_test = fillna_col_val(
        X_test, X_test[["Electrical"]].mode().values[0, 0], ["Electrical"]
    )

    # 'Exterior1st' 'Exterior2nd'
    print(
        df_all[["Exterior1st"]].isnull().sum()
    )  # один пропуск, поэтому просто заменим на моду
    print(
        df_all[["Exterior2nd"]].isnull().sum()
    )  # один пропуск, поэтому просто заменим на моду
    X_train = fillna_col_val(
        X_train, X_train[["Exterior1st"]].mode().values[0, 0], ["Exterior1st"]
    )
    X_train = fillna_col_val(
        X_train, X_train[["Exterior2nd"]].mode().values[0, 0], ["Exterior2nd"]
    )
    X_test = fillna_col_val(
        X_test, X_test[["Exterior1st"]].mode().values[0, 0], ["Exterior1st"]
    )
    X_test = fillna_col_val(
        X_test, X_test[["Exterior2nd"]].mode().values[0, 0], ["Exterior2nd"]
    )

    # Fence
    print(df_all[["Fence"]].isnull().sum())  # 2348 пропусков, поэтому заменим на None
    X_train = fillna_col_val(X_train, "None", ["Fence"])
    X_test = fillna_col_val(X_test, "None", ["Fence"])

    # FireplaceQu
    print(
        df_all[["FireplaceQu"]].isnull().sum()
    )  # 1420 пропусков, поэтому заменим на None
    X_train = fillna_col_val(X_train, "None", ["FireplaceQu"])
    X_test = fillna_col_val(X_test, "None", ["FireplaceQu"])

    # Functional
    print(df_all[["Functional"]].isnull().sum())  # 2 пропуска, поэтому заменим на моду
    X_train = fillna_col_val(
        X_train, X_train[["Functional"]].mode().values[0, 0], ["Functional"]
    )
    X_test = fillna_col_val(
        X_test, X_test[["Functional"]].mode().values[0, 0], ["Functional"]
    )

    # 'GarageArea' 'GarageCars' 'GarageYrBlt' 'GarageCond' 'GarageFinish' 'GarageQual'
    # 'GarageType'
    df_all[df_all["GarageArea"].isnull()][
        [
            "GarageArea",
            "GarageCars",
            "GarageYrBlt",
            "GarageCond",
            "GarageFinish",
            "GarageQual",
            "GarageType",
        ]
    ]  # .shape #(37, 10)
    X_test.loc[1116][
        [
            "GarageArea",
            "GarageCars",
            "GarageYrBlt",
            "GarageCond",
            "GarageFinish",
            "GarageQual",
            "GarageType",
        ]
    ]  # странный выброс на тесте

    # указан только тип, возможно это случайно, т.к. другой информации
    # по гаражу вообще нет.
    # считаем эту строку как отстуствие гаража вовсе
    X_test.loc[1116, "GarageType"] = np.nan
    X_train = fillna_col_val(X_train, 0, ["GarageArea", "GarageCars", "GarageYrBlt"])
    X_train = fillna_col_val(
        X_train, "None", ["GarageCond", "GarageFinish", "GarageQual", "GarageType"]
    )
    X_test = fillna_col_val(X_test, 0, ["GarageArea", "GarageCars", "GarageYrBlt"])
    X_test = fillna_col_val(
        X_test, "None", ["GarageCond", "GarageFinish", "GarageQual", "GarageType"]
    )

    # KitchenQual
    print(df_all[["KitchenQual"]].isnull().sum())  # 1 пропуск, поэтому заменим на моду
    X_train = fillna_col_val(
        X_train, X_train[["KitchenQual"]].mode().values[0, 0], ["KitchenQual"]
    )
    X_test = fillna_col_val(
        X_test, X_test[["KitchenQual"]].mode().values[0, 0], ["KitchenQual"]
    )

    # MSZoning
    print(
        df_all[["MSZoning"]].isnull().sum()
    )  # 4 пропуска в тесте, поэтому заменим на моду, но в разрезе локации Neighborhood
    for i in X_test["Neighborhood"].unique():
        if X_test.MSZoning[X_test["Neighborhood"] == i].isnull().sum() > 0:
            X_test.loc[X_test["Neighborhood"] == i, "MSZoning"] = X_test.loc[
                X_test["Neighborhood"] == i, "MSZoning"
            ].fillna(X_test.loc[X_test["Neighborhood"] == i, "MSZoning"].mode()[0])

            # MasVnrType 'MasVnrArea' связь такая, что None - 0, NaN - NaN, заменим
            # тогда на соответствующие значения
    df_all[df_all["MasVnrType"].isnull()][["MasVnrType", "MasVnrArea"]]
    print(
        df_all[["MasVnrType"]].isnull().sum()
    )  # 24 пропуск, заменим их значением None которое есть в справочнике на поле
    print(
        df_all[["MasVnrArea"]].isnull().sum()
    )  # 24 пропуск, заменим их значением None которое есть в справочнике на поле
    X_train = fillna_col_val(X_train, "None", ["MasVnrType"])
    X_train = fillna_col_val(X_train, 0, ["MasVnrArea"])
    X_test = fillna_col_val(X_test, "None", ["MasVnrType"])
    X_test = fillna_col_val(X_test, 0, ["MasVnrArea"])

    # TotalBsmtSF
    print(df_all[["TotalBsmtSF"]].isnull().sum())  # 1 пропуск, заменим его медианой
    X_train = fillna_col_val(
        X_train, X_train[["TotalBsmtSF"]].median().values[0], ["TotalBsmtSF"]
    )
    X_test = fillna_col_val(
        X_test, X_test[["TotalBsmtSF"]].median().values[0], ["TotalBsmtSF"]
    )

    # MiscFeature
    print(df_all[["MiscFeature"]].isnull().sum())  # 2814 пропуск
    X_train = fillna_col_val(X_train, "None", ["MiscFeature"])
    X_test = fillna_col_val(X_test, "None", ["MiscFeature"])

    # PoolQC
    print(df_all[["PoolQC"]].isnull().sum())  # 2909 пропуск
    X_train = fillna_col_val(X_train, "None", ["PoolQC"])
    X_test = fillna_col_val(X_test, "None", ["PoolQC"])

    # SaleType
    print(df_all[["SaleType"]].isnull().sum())  # 1 пропуск, заменим его модой
    X_train = fillna_col_val(
        X_train, X_train[["SaleType"]].mode().values[0, 0], ["SaleType"]
    )
    X_test = fillna_col_val(
        X_test, X_test[["SaleType"]].mode().values[0, 0], ["SaleType"]
    )

    # Utilities
    print(df_all[["Utilities"]].isnull().sum())  # 1 пропуск, заменим его модой
    X_train = fillna_col_val(
        X_train, X_train[["Utilities"]].mode().values[0, 0], ["Utilities"]
    )
    X_test = fillna_col_val(
        X_test, X_test[["Utilities"]].mode().values[0, 0], ["Utilities"]
    )

    # LotFrontage
    print(
        df_all[["LotFrontage"]].isnull().sum()
    )  # 486 пропуск, заменим его медианой, но потом можно попробовать предсказать
    # ее через регрессию
    X_train = fillna_col_val(
        X_train, X_train[["LotFrontage"]].median().values[0], ["LotFrontage"]
    )
    X_test = fillna_col_val(
        X_test, X_test[["LotFrontage"]].median().values[0], ["LotFrontage"]
    )
    X_train.info()
    X_test.info()

    for col in [
        "ExterQual",
        "ExterCond",
        "HeatingQC",
        "KitchenQual",
        "FireplaceQu",
        "GarageQual",
        "GarageCond",
        "PoolQC",
        "BsmtQual",
        "BsmtCond",
    ]:
        X_train[col] = X_train[col].map(
            {"None": 0, "Po": 1, "Fa": 2, "TA": 3, "Gd": 4, "Ex": 5}
        )
        X_test[col] = X_test[col].map(
            {"None": 0, "Po": 1, "Fa": 2, "TA": 3, "Gd": 4, "Ex": 5}
        )

    X_train["BsmtExposure"] = X_train["BsmtExposure"].map(
        {"None": 0, "No": 1, "Mn": 2, "Av": 3, "Gd": 4}
    )
    X_test["BsmtExposure"] = X_test["BsmtExposure"].map(
        {"None": 0, "No": 1, "Mn": 2, "Av": 3, "Gd": 4}
    )
    X_train["BsmtFinType1"] = X_train["BsmtFinType1"].map(
        {"None": 0, "Unf": 1, "LwQ": 2, "Rec": 3, "BLQ": 4, "ALQ": 5, "GLQ": 6}
    )
    X_train["BsmtFinType2"] = X_train["BsmtFinType2"].map(
        {"None": 0, "Unf": 1, "LwQ": 2, "Rec": 3, "BLQ": 4, "ALQ": 5, "GLQ": 6}
    )

    X_test["BsmtFinType1"] = X_test["BsmtFinType1"].map(
        {"None": 0, "Unf": 1, "LwQ": 2, "Rec": 3, "BLQ": 4, "ALQ": 5, "GLQ": 6}
    )
    X_test["BsmtFinType2"] = X_test["BsmtFinType2"].map(
        {"None": 0, "Unf": 1, "LwQ": 2, "Rec": 3, "BLQ": 4, "ALQ": 5, "GLQ": 6}
    )

    # тут не такие очевидные категории оценкци, которые эвристически можно
    # приоритезировать
    X_train["LandSlope"] = X_train["LandSlope"].map({"Sev": 1, "Mod": 2, "Gtl": 3})
    X_train["PavedDrive"] = X_train["PavedDrive"].map({"N": 1, "P": 2, "Y": 3})
    X_train["Functional"] = X_train["Functional"].map(
        {
            "Sal": 1,
            "Sev": 2,
            "Maj2": 3,
            "Maj1": 4,
            "Mod": 5,
            "Min2": 6,
            "Min1": 7,
            "Typ": 8,
        }
    )
    X_train["Street"] = X_train["Street"].map({"Grvl": 1, "Pave": 2})
    X_train["Utilities"] = X_train["Utilities"].map(
        {"ELO": 1, "NoSeWa": 2, "NoSewr": 3, "AllPub": 4}
    )

    X_test["LandSlope"] = X_test["LandSlope"].map({"Sev": 1, "Mod": 2, "Gtl": 3})
    X_test["PavedDrive"] = X_test["PavedDrive"].map({"N": 1, "P": 2, "Y": 3})
    X_test["Functional"] = X_test["Functional"].map(
        {
            "Sal": 1,
            "Sev": 2,
            "Maj2": 3,
            "Maj1": 4,
            "Mod": 5,
            "Min2": 6,
            "Min1": 7,
            "Typ": 8,
        }
    )
    X_test["Street"] = X_test["Street"].map({"Grvl": 1, "Pave": 2})
    X_test["Utilities"] = X_test["Utilities"].map(
        {"ELO": 1, "NoSeWa": 2, "NoSewr": 3, "AllPub": 4}
    )

    # это категориальная фича, переведем ее в текст, чтобы потом быстро отделить
    # категории от чисел
    X_train["MSSubClass"] = X_train["MSSubClass"].map(
        {
            20: "class1",
            30: "class2",
            40: "class3",
            45: "class4",
            50: "class5",
            60: "class6",
            70: "class7",
            75: "class8",
            80: "class9",
            85: "class10",
            90: "class11",
            120: "class12",
            150: "class13",
            160: "class14",
            180: "class15",
            190: "class16",
        }
    )
    X_test["MSSubClass"] = X_test["MSSubClass"].map(
        {
            20: "class1",
            30: "class2",
            40: "class3",
            45: "class4",
            50: "class5",
            60: "class6",
            70: "class7",
            75: "class8",
            80: "class9",
            85: "class10",
            90: "class11",
            120: "class12",
            150: "class13",
            160: "class14",
            180: "class15",
            190: "class16",
        }
    )

    # отделим категориальные фичи от числовых
    X_train_num = X_train.select_dtypes(include="number")
    X_test_num = X_test.select_dtypes(include="number")
    X_train_cat = X_train.select_dtypes(exclude="number")
    X_test_cat = X_test.select_dtypes(exclude="number")

    # отмасштабируем числовые признаки
    X_train_num_scale, X_test_num_scale = df_scale(X_train_num, X_test_num)

    # закодируем категориальные признаки через Hashing trick, предполагая,
    # что в каждой фиче
    # 10 параметров. Возможно можно сузить
    X_train_cat_hash = get_hash_data(X_train_cat, X_train_cat.shape[1] * 10)
    X_test_cat_hash = get_hash_data(X_test_cat, X_train_cat.shape[1] * 10)

    # соберем подготовленные фичи
    X_train_all = np.hstack([X_train_cat_hash, X_train_num_scale])
    X_test_all = np.hstack([X_test_cat_hash, X_test_num_scale])
    X_train_all.shape, X_test_all.shape

    X_train.info()
    X_test.info()


# Press the green button in the gutter to run the script.
if __name__ == "__main__":
    print("PyCharm")
    load_data()
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
