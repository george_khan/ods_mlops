from src import load_data
import pytest
from click.testing import CliRunner
import pandas as pd
import great_expectations as ge

runner = CliRunner()


def test_cli_command():
    result = runner.invoke(load_data, "data/raw/train.csv data/raw/test.csv")
    return result.exit_code == 0


def test_output():
    df_train = pd.read_csv("data/raw/train.csv")
    df_test = pd.read_csv("data/raw/test.csv")

    df_train_ge = ge.from_pandas(df_train)
    df_test_ge = ge.from_pandas(df_test)

    expected_columns_test = [
        "Id",
        "MSSubClass",
        "MSZoning",
        "LotFrontage",
        "LotArea",
        "Street",
        "Alley",
        "LotShape",
        "LandContour",
        "Utilities",
        "LotConfig",
        "LandSlope",
        "Neighborhood",
        "Condition1",
        "Condition2",
        "BldgType",
        "HouseStyle",
        "OverallQual",
        "OverallCond",
        "YearBuilt",
        "YearRemodAdd",
        "RoofStyle",
        "RoofMatl",
        "Exterior1st",
        "Exterior2nd",
        "MasVnrType",
        "MasVnrArea",
        "ExterQual",
        "ExterCond",
        "Foundation",
        "BsmtQual",
        "BsmtCond",
        "BsmtExposure",
        "BsmtFinType1",
        "BsmtFinSF1",
        "BsmtFinType2",
        "BsmtFinSF2",
        "BsmtUnfSF",
        "TotalBsmtSF",
        "Heating",
        "HeatingQC",
        "CentralAir",
        "Electrical",
        "1stFlrSF",
        "2ndFlrSF",
        "LowQualFinSF",
        "GrLivArea",
        "BsmtFullBath",
        "BsmtHalfBath",
        "FullBath",
        "HalfBath",
        "BedroomAbvGr",
        "KitchenAbvGr",
        "KitchenQual",
        "TotRmsAbvGrd",
        "Functional",
        "Fireplaces",
        "FireplaceQu",
        "GarageType",
        "GarageYrBlt",
        "GarageFinish",
        "GarageCars",
        "GarageArea",
        "GarageQual",
        "GarageCond",
        "PavedDrive",
        "WoodDeckSF",
        "OpenPorchSF",
        "EnclosedPorch",
        "3SsnPorch",
        "ScreenPorch",
        "PoolArea",
        "PoolQC",
        "Fence",
        "MiscFeature",
        "MiscVal",
        "MoSold",
        "YrSold",
        "SaleType",
        "SaleCondition",
    ]
    expected_columns_train = expected_columns_test + ["SalePrice"]

    assert (
        df_train_ge.expect_table_columns_to_match_ordered_list(
            column_list=expected_columns_train
        ).success
        is True
    )
    assert (
        df_test_ge.expect_table_columns_to_match_ordered_list(
            column_list=expected_columns_test
        ).success
        is True
    )
