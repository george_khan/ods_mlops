from src import prepare_cat_features
import pytest
from click.testing import CliRunner

runner = CliRunner()


def test_cli_command():
    result = runner.invoke(
        prepare_cat_features, "data/interim/train_clean.csv data/interim/test_clean.csv"
    )
    return result.exit_code == 0
