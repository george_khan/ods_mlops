from src import make_result_data
import pytest
from click.testing import CliRunner

runner = CliRunner()


def test_cli_command():
    result = runner.invoke(
        make_result_data,
        "data/interim/train_prepared_num.csv data/interim/train_prepared_cat.pickle\
        data/interim/test_prepared_num.csv data/interim/test_prepared_cat.pickle",
    )
    return result.exit_code == 0
