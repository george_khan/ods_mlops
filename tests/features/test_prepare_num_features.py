from src import prepare_num_features
import pytest
from click.testing import CliRunner
import pandas as pd
import great_expectations as ge

runner = CliRunner()


def test_cli_command():
    result = runner.invoke(
        prepare_num_features, "data/interim/train_clean.csv data/interim/test_clean.csv"
    )
    return result.exit_code == 0


def test_output():
    df_train = pd.read_csv("data/interim/train_prepared_num.csv")
    df_test = pd.read_csv("data/interim/test_prepared_num.csv")

    df_train_ge = ge.from_pandas(df_train)
    df_test_ge = ge.from_pandas(df_test)

    expected_columns = [
        "LotFrontage",
        "LotArea",
        "Street",
        "Utilities",
        "LandSlope",
        "OverallQual",
        "OverallCond",
        "YearBuilt",
        "YearRemodAdd",
        "MasVnrArea",
        "ExterQual",
        "ExterCond",
        "BsmtFinSF1",
        "BsmtFinSF2",
        "BsmtUnfSF",
        "TotalBsmtSF",
        "HeatingQC",
        "1stFlrSF",
        "2ndFlrSF",
        "LowQualFinSF",
        "GrLivArea",
        "BsmtFullBath",
        "BsmtHalfBath",
        "FullBath",
        "HalfBath",
        "BedroomAbvGr",
        "KitchenAbvGr",
        "KitchenQual",
        "TotRmsAbvGrd",
        "Functional",
        "Fireplaces",
        "GarageYrBlt",
        "GarageCars",
        "GarageArea",
        "PavedDrive",
        "WoodDeckSF",
        "OpenPorchSF",
        "EnclosedPorch",
        "3SsnPorch",
        "ScreenPorch",
        "PoolArea",
        "MiscVal",
        "MoSold",
        "YrSold",
    ]

    assert (
        df_train_ge.expect_table_columns_to_match_ordered_list(
            column_list=expected_columns
        ).success
        is True
    )
    assert (
        df_test_ge.expect_table_columns_to_match_ordered_list(
            column_list=expected_columns
        ).success
        is True
    )

    for column in expected_columns:
        assert (
            df_train_ge.expect_column_values_to_be_of_type(
                column=column, type_="float64"
            ).success
            is True
        )
        assert (
            df_test_ge.expect_column_values_to_be_of_type(
                column=column, type_="float64"
            ).success
            is True
        )
