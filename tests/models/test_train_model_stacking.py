from src import fit_stacking
import pytest
from click.testing import CliRunner
import pandas as pd
import great_expectations as ge

runner = CliRunner()


def test_cli_command():
    result = runner.invoke(
        fit_stacking,
        "data/processed/train.pickle data/processed/test.pickle data/processed/target.csv\
         data/processed/test_idx.csv models/kernel_ridge.pkl",
    )
    return result.exit_code == 0


def test_output():
    predict = pd.read_csv("data/processed/house_price_stacking.csv")

    predict_ge = ge.from_pandas(predict)

    expected_columns = ["Id", "SalePrice"]

    assert (
        predict_ge.expect_table_columns_to_match_ordered_list(
            column_list=expected_columns
        ).success
        is True
    )

    for column in expected_columns:
        assert (
            predict_ge.expect_column_values_to_not_be_null(column=column).success
            is True
        )
